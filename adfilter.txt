[Adblock Plus 2.0]
! Title: Agressive Annoyances and AdFiltering
! Author: Markus Göllnitz a.k.a. camelCaseNick | bewares.it
! Version: 12.0
! Expires: 3 days
! Description: Blocks agressively advertisements and other annoying elements, such as teasers, subscription offers, calls for donation etc.
! License: https://creativecommons.org/licenses/by-sa/4.0/
! Homepage: http://bewares.it/adfilter
! Contact: http://bewares.it/imprint
! WritingRules: https://adblockplus.org/filters
! RedundantRules: https://arestwo.org/famlam/redundantRuleChecker.html

!! tracking

||ioam.de^$third-party

||geolocation.stroeerdp.de^$important

||fbcdn.net^$domain=~facebook.com
||facebook.$domain=~facebook.com

!! advertisments

##.adplace

##[class*="sponsoredcontent"]

##[href*="://order.cyon.ch/affiliate/"]

||serve.adpresenter.de^$third-party

||linksmanagement.com/wp-content/uploads/*.gif$image

||heroesofrpg.com^$document

!!!!!!!!!!!! SITE BASED !!!!!!!!!!!!

!! general interest magazine germany

focus.de###sidebar > * > :not(.header):not(.content):not(.footer)
focus.de###page-container > :not(#main):not(.container_12):not(#center-banner):not(script)
focus.de###fancybox-wrap
!focus.de###main > .grid_4

stern.de###page > :not(#main-wrapper)
stern.de###main-wrapper > div
stern.de##.article__page-end > :not(section):not(.html-container):not([class^="o-"])
stern.de##main > div:not(.html-container):not([class^="o-"]):not(.article__page-end)
stern.de##.with_ad
stern.de##.ligatusWidget[class*="_RUN"]
!stern.de##sbsticky > :not(.sb-box):not(h3)
!stern.de##.article-content > :not([class^="m-"]):not([class^="o-"]):not([itemprop="articleBody"])
!||stern.de/public/images/fallback^$image

www.mopo.de##.dm_ad-leaderboard.dm_ad
||www.mopo.de/ajax/*/asFriendlyIFrameScript$subdocument
||elbe.mopo.de/jobs/index.php$subdocument

spiegel.de##.asset-box[data-vr-zone="Listbox: Anzeige"]
!spiegel.de###article-overscroll-area
!spiegel.de###content-main > div[class="clearfix"]:not([id])

!! newspapers germany

welt.de##[class*="premium-icon"]
welt.de##a[name$="_a_plus_"]
welt.de##a[name$="_a_plus_"] + [data-qa="Teaser.Body"]
welt.de##article:has(a[name$="_a_plus_"])
welt.de##section.c-stage[data-tb-region="weltplus"]
welt.de##[data-qa="Commercial.label"]
welt.de##[data-qa="TaboolaComponent"]
welt.de##[data-qa="Stage.InhouseAd"]
welt.de##[class$="--has-commercials"]
welt.de##[class*="--has-commercials "]
welt.de##[class$="--is-commercial"]
welt.de##[class*="--is-commercial "]
welt.de##a[href*="abo.welt.de"]
welt.de##a[href^="/Advertorials"]
welt.de##a[href*="/plus"]
welt.de##.c-grid__item:not(:has(article:not(:has(a[name$="_a_plus_"]))))
welt.de##.c-inline-teaser.o-teaser:contains(Exklusiv für Abonnenten)
||cdn.taboola.com^$script,domain=welt.de
||welt.de/Advertorials$document
!welt.de###cobiFeed

sueddeutsche.de##.interactiveeditorial

m.bild.de##.hentry:has(a):not(:has(a:not([href*="bild-plus"])))
bild.de##section:has(a):not(:has(a:not([href*="bild-plus"])))
www.bild.de##[data-tb-region-item-premium]
www.bild.de##section:has(a:contains(BILDplus)):not(:contains(Meldung))
www.bild.de##section:has(li:contains(ANZEIGE))
www.bild.de##[href*="/bild-plus/"]
www.bild.de##.rssFeed_seo_widget span:contains(BILDplus)
www.bild.de##.hentry:has(a[href*="travelbook.de"])
www.bild.de##.misc:has([href*="/bild-plus/"]):not(:contains(.))
!@@$domain=bild.de

merkur.de##[class*="--proBEEP "]

handelsblatt.com##.vhb-type-premium
handelsblatt.com##.vhb-indicator
handelsblatt.com##a[href*="handelsblatt.com/adv"]
handelsblatt.com##a[href^="/my"]

abendblatt.de##.has-anzeige

bergedorfer-zeitung.de##article.advertisement
bergedorfer-zeitung.de##a[href*="//anzeigen.bergedorfer-zeitung.de"]
bergedorfer-zeitung.de##a[href*="bergedorfer-zeitung.de/anzeigen"]

rp-online.de##[data-portal-id]

faz.net##article:has([class*="FazPlus"])
faz.net##a[href*="plus"]

www.journalistenwatch.com###yes-invest-inhalte
www.journalistenwatch.com##[href="https://www.fincabayano.net/auswandern/einleitung/"]
www.journalistenwatch.com###yes-invest-inhalte-c2
||www.journalistenwatch.com/wp-content/uploads/2018/11/cover.jpg$image
||www.journalistenwatch.com/wp-content/uploads/2016/11/pax-europa.gif$image

!! general interest portals germany

t-online.de###T-brTargtg-cont
t-online.de###Tlibelle
t-online.de###Tgb-73315542
t-online.de###Tgboxf
t-online.de##.Tdivmap[style]
t-online.de##a[href*="magentacloud.de"]
t-online.de##a[href*="t-mobile.de"]
t-online.de##a[href*="xiti.com"]
t-online.de##a[href*="eces.t-online.de"]
t-online.de##a[href*="telekom.de"]
t-online.de##a[href*="jackpot.de"]
t-online.de##a[href*="billiger.de"]
t-online.de##a[href*="erotic-lounge.com"]
t-online.de##a[href*="hotspot.de"]
t-online.de##a[href*="lotto.t-online.de"]
t-online.de##a[href*="mydoc.de"]
t-online.de##a[href*="maggi.de/"]
t-online.de##a[href*="//tarife-und-produkte.t-online.de"]
t-online.de##a[href*="//tarifbestellen.t-online.de"]
t-online.de##.taAnmelden
t-online.de##.Tgbox:has([id^="imTocbmini_pos"])
t-online.de###Tasfeed1a
t-online.de##[id^="nativendo-infeed"]
t-online.de##.Tmm:has(a[href*="count.shopping.t-online.de"])
t-online.de###T-shoppingbox
||bilder.t-online.de/b/84/75/16/02/id_84751602/tid_da/index.jpg$image
!t-online.de##.Tadblock
!t-online.de###Ttub1

www.bento.de##body > :not(#wrapper)
www.bento.de###content > :not(main)
www.bento.de##.contentBlock-generated_ad
www.bento.de##.aobj6.container > :not(.contentBlock):not(.opener)
www.bento.de##footer > div:has(img):not(:has(:not(img)))

!! topic based journals germany

brigitte.de##.navigation-primary-flyout .navigation-link > .flyout-inner-default__add-on
brigitte.de##.navigation-primary-flyout .navigation-link > a[data-clickcommand]
brigitte.de##a[href*="//brigitte.parship.de"]

tvspielfilm.de###main > :not(.content-area)
tvspielfilm.de##aside > :not(article):not(style):not(.widget):not(.htmlBlock)
tvspielfilm.de###jumphome
tvspielfilm.de##a[href*="live.tvspielfilm.de"]
tvspielfilm.de##a[href*="tvspielfilm-abo.de"]
tvspielfilm.de##.live-logo
!tvspielfilm.de##.livetv-rollin
!tvspielfilm.de##.advertising-box
!tvspielfilm.de###wrapper > a.copyright
!tvspielfilm.de##.two-blocks:not(.two-blocks-2) > div:not(.posts-column)

golem.de###gts-clip
golem.de###pricehits
golem.de###gservices
golem.de###job-market
golem.de##li[style="min-height:4em; padding-left:0;"]
forum.golem.de##article > div[style="min-height:266px; margin:20px 0 10px;"]
||www.golem.de/_img/161031-sys11junior.jpg$image
||www.golem.de/microsite/abo/*.shtml$subdocument
!golem.de###screen > :not(.g):not(#index-promo):not(hr):not(noscript)

heise.de##a[href*="//shop.heise.de"]
heise.de##a[href*="/download/deal-des-tages"]
heise.de##a[href*="//spiele.heise.de/"]
heise.de##a[href*="//jobs.heise.de/"]
heise.de##aside[aria-label="heise print magazines"]
heise.de##.stage--aside.teaser_vertrieb_events
heise.de###jobware-teaser
heise.de##.teaser_hbs
heise.de##.teaser_deal_des_tages
heise.de##.teaser_preisvergleich_top10
heise.de##article.a-article-teaser:has([class*="heiseplus"])
heise.de##article [class*="heiseplus"]
heise.de###mitte_rechts a-ad
heise.de##.stage--heiseplus.stage

netzwelt.de##a[href*="ipvanish.com"]

giga.de##.u-shape-top
giga.de##.ed-container-rectangle
giga.de##.coupon
giga.de##article.teaser.teaser-advertisement
giga.de###teaserheld-header
giga.de##nav:style(margin-top: 0 !important)
giga.de##body:style(padding-top: 9.6rem !important)
||teaser.giga.de/44/b7/0c/fc0f080fcc2ee03cf198827809_AzQ3ZmYwY2VjOTIw_b79430d3e008fcdff21108b70879d947.jpeg$image
||www.giga.de/special/gutscheine/bg.jpg$image
!giga.de outbrain, fireplace
!giga.de##.outbrain
!||giga.de/spx^$image,script
!||giga.de/*/outbrain$script

amazona.de###premiumbanner
amazona.de##[class$="-ad"],[class*="-ad "]

t3n.de##[class*="sponsoredcontent"]

!! online shops

notebooksbilliger.de##.skyscraper
notebooksbilliger.de##.banner_top

gearbest.com##.back-top-banner
gearbest.com##.siteBanner
||uidesign.gbtcdn.com/GB/images/promotion/$image

aliexpress.com##.top-banner-container
||ae01.alicdn.com/kf/HTB1JlDYLMTqK1RjSZPhq6xfOFXa6.jpg$image

ebay.de##a[href^="https://pulsar.ebay.de/plsr/clk/"]
ebay.de##.promoted-lv
ebay.de###gh-p-1
ebay.de###gh-ti
ebay.de###gh-plus
ebay.de###gh-hsi
||ir.ebaystatic.com/cr/v/c1/EB451_Elec_Bware_Doodle_150x30.png$image
||ir.ebaystatic.com/cr/v/c1/EB1255_DE_MEDION_10BirthdayCoupon_Doodle_150x30.jpg$image
||ir.ebaystatic.com/cr/v/c1/EB-543_DE_Fashion_Winterfashioncoupon_Doodle.jpg$image
||ir.ebaystatic.com/cr/v/c1/EB-630_DE_C2C_SeSu2810_Doodle_150x30.jpg$image
||ir.ebaystatic.com/cr/v/c1/16525_DE_Elec_MedimaxCoupon_Doodle_150x30.png$image
||ir.ebaystatic.com/cr/v/c1/74619_mmddyy_sweet16deals_doodle_150x30_final.jpg$image

ebay-kleinanzeigen.de###srchrslt-adtable-topads
ebay-kleinanzeigen.de##.native-ad > .aditem
ebay-kleinanzeigen.de##.is-highlight > .aditem
ebay-kleinanzeigen.de###home-gallery
ebay-kleinanzeigen.de###vap_adsense-middle
ebay-kleinanzeigen.de###vap_adsense-bottom
||ebay-kleinanzeigen.de/static/js/reinsert-all.c*.js^$script
!ebay-kleinanzeigen.de##.native-ad
!ebay-kleinanzeigen.de##.treebay
!ebay-kleinanzeigen.de##.indeed
!||ebay-kleinanzeigen.de/static/js/base.*.js^$script
!||ebay-kleinanzeigen.de/static/js/top.unxrezymtbqc.js^$script

amazon.de###nav-upnav
||images-eu.ssl-images-amazon.com/images/G/03/marketing/prime/Student/DE_Amazon-Student-SWM_Dsktp-GW_400x39._CB475342321_.png$image

amazon.*##.nav-prime-try.nav-logo-tagline.nav-sprite
amazon.*###navSwmHoliday
amazon.*###gw-desktop-herotator
amazon.*###ape_Gateway_right-2_desktop_placement

!! online services

wetteronline.de##.flagged
wetteronline.de##.recFlag
wetteronline.de###topcontainer
wetteronline.de###rightcontainer
wetteronline.de###woLead
wetteronline.de###header + :not(#content)
wetteronline.de###sidebar > :not([id]):not(.leftflow):not(.teaser_block)
wetteronline.de###sidebar  [id*="-"]:not(li)
wetteronline.de###product_display div:has(iframe)
||st.wetteronline.de/dr/1.1.76/images/icon/wetteronline_home_neu.png$image
||cnt.wetteronline.de$~document
!||*.goaz.wetteronline.de/$subdocument

yabeat.com###text-banner
yabeat.com###smartBanner
yabeat.com###inters
yabeat.com###footer > .right
yabeat.com##.adp-native-list
yabeat.com##.banner

socialblade.com##div[style*="padding: 20px"]:has(.socialblade-heartbeat)

stats.wikimedia.org##.error.notice.central

stackshare.io##[id^="SponsoredTool-"]

!! steaming platforms

youtube.com##.ytd-banner-promo-renderer
youtube.com##ytd-compact-promoted-video-renderer
youtube.com###masthead-ad

!! online reference work

wikipedia.org###centralNotice.cn-fundraising

albertmartin.de###ad_rs_sky
albertmartin.de##a[href*="empfehlungdestages.de"]
||albertmartin.de/latein/inc/empfehlung.php^$xmlhttprequest

dict.leo.org##.tf1.p.fl-right
dict.leo.org##.rotateAdText

duden.de###block-dudende-d-970x250-1billboard
duden.de###block-dudende-d-300x600-1sidebarobensticky
duden.de###block-dudende-d-300x250-1sidebarunten
duden.de##main aside

dejure.org###stellenanzeigen
dejure.org##.googlediv
||j2.dejure.org/jcg/anzeige_*.svg$image

!! social media

twitter.com##[aria-label="Wem folgen?"] [role="button"]:contains(Gesponsert)
!twitter##[class*="-promoted-"]
!twitter##[class^="promoted-"]
!twitter##[class$="-promoted"]

!! topic blogs, minor sites and regional content

naturinspiriert.org##.ad.right

||woodworker.de/img_banner/anzeige.gif$image
!woodworker.de##table table div[align="center"] > div.page > div > :not([id]) > table

webdesign.tutsplus.com##.orange.free-trial-highlight
||static.tutsplus.com/assets/free_trial/*.png$image

www.hamburg.de##[class^="col-"]:has(.sponsored:contains(Anzeige))

||pixelio.de/imgs/publi/*.jpg$image

www.chefblogger.me##[href*="://www.elegantthemes.com/affiliates/"]
||www.chefblogger.me/banner/ad-cyon.png
||www.chefblogger.me/wp-content/uploads/2019/02/elegantthemes300x250.gif

!! forums

ifixit.com###answersBanner