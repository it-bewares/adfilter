# it.bewares adFilters

[subscribe](abp:subscribe?location=https://gitlab.com/it-bewares/adfilter/raw/master/adfilter.txt&title=Agressive%20Annoyances%20and%20AdFiltering)

Beware of advertisements, tracking and other annoying stuff.

To support you with that it blocks agressively advertisements and other annoying elements, such as teasers, subscription offers, calls for donation etc.

- I try my best at preserving functionality, but I prefer a partly broken website over displayed ads.
- I filter what I find and find disturbing. Contact me if something is broken and you think it shouldn't bother me to change that. Maybe I just don't visit that site as frequent as you do.
- I mainly surf in German and English. Do not expect anything outside of my web bubble to be filtered.

Expect all filter lists to be licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/) and software under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl)


[about it](http://bewares.it/adfilter)
